# Adventures with GPU passthroughs and virtualization

## Background

I have a small server rack in the closet in my office. It houses a few different servers built with old hardware and a couple SBCs.

My current server configuration is the following:

- In-use in the rack
    - NAS (openmediavault)
        - Intel Atom D510
        - 4GB DDR2
        - 32GB Sandisk boot drive
        - 2x 2TB hard drives in a ZFS array

    - VM Server (Proxmox)
        - AMD Opteron 6278 @ 2.8Ghz (will explain later)
        - 64GB DDR3
        - 500GB Crucial MX500 SSD
        - A couple hot swap drive bays

    - Gaming server (Windows 10 & Parsec)
        - AMD Phenom ii x6 1055t
        - 8GB DDR3
        - Nvidia GTX 1650 (non-super or OC)
        - 64GB mSata SSD
        - Old 640GB WD blue for storing games

- In the rack but not in use
    - Marvell espressobin
        - Was going to make this an IPMI controller for my VM server, and while I got it working sort of, the espressobin is probably my least favorite system I have ever used so it's no longer in use
    - RasperryPi 3
        - Central brain for motioneyeos, but I need to put together a better system before I spin this back up

Nothing too crazy here. My VM server is used mostly to have something like a mini cloud to play around with different technologies and software, 
I play mostly retro games and Fortnite on my gaming server, and my NAS is just periodic storage.

## So, what's the problem here?

Nothing really other than inefficient power consumption. My VM server is not used for much other than a couple linux VMs and the 64GB of RAM is kinda just twiddling its thumbs. 

My gaming server is a single use, somewhat power hungry system. The board for the gaming server is also not in the best shape so only two DIMMs work which limits any memory expansion.
So, I figured since I have an available PCIe x16 slot in my VM server, why not try to port the whole gaming server into a VM and pass through the GPU to it?

**TL;DR: You can definitely pass through a GPU to a VM with some success, but it's a configuration nightmare and not too useable (for now)**

Before I started tearing apart my server rack for this weird experiment, I wanted to get a quick benchmark of my gaming server. It scored a 2290 in Unigine Valley which is a fairly dated, not multi-core optimized GPU benchmarking application. That is the number to beat, or at least reach on the VM.

## Great, let's virtualize all the things

*insert montage of all the hardware moving around and configuring the BIOS for the first time*

I found a guide on the homelab subreddit on how to set up proxmox for passing a GPU into a proxmox VM. Long story short, the guide is phenomenal and cover about 95% of the process.

I initially set up a VM with 6 cores and 16GB of RAM to try to replicate the gaming server. With 64GB of RAM at my disposal, I figured I could spare the extra 8 gigs.

Installing the Nvidia drivers was really straightforward and I didn't run into any issues just firing up the VM with the PCI device attached. I had a display plugged into the GTX 1650 so the GPU would actually be used, but that's currently how I handle Parsec'ing into my gaming server. 

After following the guide, installing the GPU drivers, and loading up some GPU benchmarks, it was time to benchmark the new system.

...and it scored something like 1490 in Unigine valley. Clock for clock, the opteron was slower than the phenom. 2.4Ghz vs 2.8Ghz. Okay, let's first try adding more cores (I had another 10 around).

Still in the 1500 range.

### ...Can I overclock my server?

To my surprise, I could with the help of a third party firmware known as OCNG5. The process in the OCNG blog was straightforward and I was able to overclock the system to 2.8Ghz. Perfect, now we should see some larger improvements, right?

### Wrong

The best score I could get in the valley benchmark was 1700 once. Regardless of the configuration, I was getting between 1490 and 1600.
I was able to run Unigine Superposition on the VM, which I couldn't run on the gaming server, and there was a noticeable improvement between overclocking settings since Superposition is a more modern benchmark application.

## Well how about in an actual game?

Maybe the benchmarking software was pushing the system to its limits? Why not try this in one of the games I plan to play?

So, I loaded up Fortnite, let it automatically set the settings, and played a game.

**~20fps**

Well, that's pretty much unplayeable. I was getting well above 60fps on my dedicated server on medium-high settings at 1440p

## Enter the configuration nightmare

I spent another couple days trying to figure out why the performance was so bad. I tried overclocking the GPU with no improvements, tried overclocking the CPU more with no improvements, and spent way too much time reading about the inner workings of QEMU and VFIO only to still barely understand how it all works.

Through all this digging, I noticed GPU-Z had a strange little piece of information: I was running my GPU at PCIe 1.1 x2. That's 0.5 GB/s vs 4 GB/s my PCIe x16 slot running at x8 should be able to handle. I double checked this with `lspci -vv` and sure enough, it was running at x2.

I tried another round of different configuration settings and was getting nowhere with it. x2 regardless of what I did. I disabled one of my other PCIe slots that shares some bandwidth with the slot I'm using to see if anything changed. Nothing.

Then I figured I could read through the manual of my motherboard. Maybe I'm missing something here?

And I was. There is a set of jumpers disabling I2C on my PCIe x16 slot. I moved the jumpers to enable I2C, and voila, GPU-Z and lspci were reporting the card was running at x8.

Success! Right?

Well, no. Nothing changed.

## Well this was a disappointment of a story

I agree. I learned a lot from this, but it was a lackluster result.

However, I think I might still have some options left here. Apprently I allocated too many CPU cores for NUMA to properly allocate CPU resources to the VM. 

## Sources

- Main guide
    - https://www.reddit.com/r/homelab/comments/b5xpua/the_ultimate_beginners_guide_to_gpu_passthrough/
- Overclocking my Opteron system
    - http://area51dev.blogspot.com/
- Other links
    - https://support.parsec.app/hc/en-us/articles/360049148651-Connect-to-the-Log-On-Screen-in-Windows
    - https://www.reddit.com/r/Proxmox/comments/fakouk/proxmox_gpu_passthrough_works_but_performance_is/
    - https://forum.proxmox.com/threads/legacy-bios-to-uefi-mode.72601/
    - http://vfio.blogspot.com/2014/08/vfiovga-faq.html
    - http://vfio.blogspot.com/2014/08/iommu-groups-inside-and-out.html
    - https://www.reddit.com/r/VFIO/comments/9ntlwv/increasing_vfio_vga_performance_linux/
    - https://forum.proxmox.com/threads/adding-pci-express-root-bus-device-to-the-config-file.74312/
    - https://github.com/qemu/qemu/blob/master/docs/pcie.txt
    - https://pve.proxmox.com/pve-docs/qm.1.html
    - https://forum.proxmox.com/threads/cant-start-kvm-machine-exit-code-1.20729/
    - https://pve.proxmox.com/wiki/PCI(e)_Passthrough
    - https://forum.proxmox.com/threads/gpu-passthrough-on-amd-machine.34932/
    - https://forum.proxmox.com/threads/nvidia-750ti-vga-passthrough-no-hdmi-audio.22088/
    - https://access.redhat.com/documentation/en-us/red_hat_virtualization/4.1/html/installation_guide/appe-configuring_a_hypervisor_host_for_pci_passthrough
    - https://pve.proxmox.com/wiki/Manual:_qm.conf
    - https://askubuntu.com/questions/207175/what-does-nomodeset-do
    - https://unix.stackexchange.com/questions/393/how-to-check-how-many-lanes-are-used-by-the-pcie-card
    - https://pve.proxmox.com/wiki/NUMA